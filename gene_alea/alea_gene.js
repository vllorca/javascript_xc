'use strict';

function bornes_frequence_alea(rand_max, taille) {
    let result = new Array(rand_max)
    for (let i = 0; i < result.length; i++) {
        result[i] = 0
    }
    let bornes = { min: taille, max: 0, ecart: 0 }

    for (let i = 0; i < taille; i++) {
        let x = Math.floor(Math.random() * rand_max);
        ++result[x];
    }

    result.forEach(elem => {
        if (elem > bornes.max) {
            bornes.max = elem;
        }
        else if (elem < bornes.min) {
            bornes.min = elem
        }
        bornes.ecart = bornes.max - bornes.min
    })
    ListToTable(result);
    return bornes
}

function iter_ecart(rand_max, taille, nb_iter) {
    let experienceName = document.createElement('h3')
    experienceName.innerHTML = "Experience sur le nombre d\'iteration :"
    document.body.appendChild(experienceName)

    let experience = {}
    let ecart = { min: taille, max: 0, moyenne: 0 }

    for (let i = 0; i < nb_iter; i++) {
        experience = bornes_frequence_alea(rand_max, taille)

        if (experience.ecart > ecart.max) {
            ecart.max = experience.ecart
        } else if (experience.ecart < ecart.min) {
            ecart.min = experience.ecart
        }
        ecart.moyenne += experience.ecart

    }
    ecart.moyenne /= nb_iter

    ListToTable(ecart)

    return ecart

}

function iter_taille(rand_max, taille) {
    let experienceName = document.createElement('h3')
    experienceName.innerHTML = "Experience sur la taille de l'échantillon :"
    document.body.appendChild(experienceName)

    let experience = { min: rand_max, max: 0 }
    taille.forEach(n => {
        experience = bornes_frequence_alea(rand_max, n);
        ListToTable(experience)
        return experience
    });

}

function ListToTable(experience) {


    let keys = Object.keys(experience);
    let table = document.createElement('table')

    let tr_header = document.createElement('tr');
    table.appendChild(tr_header);
    keys.forEach(attribut => {
        let th = document.createElement('th');
        th.innerText = attribut;
        tr_header.appendChild(th);
    })

    let tr_data = document.createElement('tr');
    table.appendChild(tr_data);
    keys.forEach(attribut => {
        let td = document.createElement('td');
        td.innerText = experience[attribut];
        tr_data.appendChild(td);
    })
    document.body.appendChild(table)

    var br = document.createElement("br");
    document.body.appendChild(br)
}



console.log(iter_ecart(10, 200, 10))
console.log(iter_taille(10, [50, 60, 70]))


